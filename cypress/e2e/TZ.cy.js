Cypress.on('uncaught:exception', (err, runnable) => {
  return false;
});

it('first test', () => {
  const checkButtons = () => {
    cy.get('#enableAfter').should('have.attr', 'disabled'); //проверка что атрибут disabled есть
    cy.wait(5000);
    cy.get('#enableAfter').should('not.have.attr', 'disabled'); //проверка что атрибута disabled нету после прошествия 5 секунд
    cy.get('button').contains('Visible After 5 Seconds').should('be.visible');
  };
  cy.visit('/dynamic-properties');
  checkButtons();
  cy.reload(true);
  checkButtons(); //проверка что после обновелния страницы все работает так же
});

it('second test', () => {
  cy.viewport(1600, 800);
  cy.visit('/webtables');
  cy.get('div').contains('First Name').should('be.visible');
  cy.get('div').contains('Last Name').should('be.visible');
  cy.get('div').contains('Age').should('be.visible');
  cy.get('div').contains('Email').should('be.visible');
  cy.get('div').contains('Salary').should('be.visible');
  cy.get('div').contains('Department').should('be.visible');
  cy.get('div').contains('Action').should('be.visible');
  //проверка что нужный нам контент есть на странице
  cy.get('#addNewRecordButton').click();
  cy.get('#firstName').type('Artur');
  cy.get('#lastName').type('Basyrov');
  cy.get('#userEmail').type('DookER2020@mail.ru');
  cy.get('#age').type('31');
  cy.get('#salary').type('1500');
  cy.get('#department').type('AQA');
  cy.get('#submit').click();
  cy.get('div').contains('Basyrov').should('be.visible');
  cy.get('#delete-record-4').click();
  cy.get('div').contains('Basyrov').should('not.exist');
  //Создаем нового пользователя, проверяем что он появился у нас, удаляем и смотрим что его больше нет
  cy.get('#searchBox').type('Cierra');
  cy.get('#basic-addon2').click();
  cy.get('.rt-tbody [class="rt-tr -odd"]').should('have.length', 1);
  cy.get('div').contains('Cierra').should('be.visible');
  //проверка что осталась только одна наполненная строка c контентом Cierra

  cy.get('#addNewRecordButton').click();
  cy.get('#firstName').type('Artur');
  cy.get('div#registration-form-modal')
    .contains('Registration Form')
    .should('be.visible');
  //проверка что модальное окно не закрыто после клика кнопки 'submit'
});

it('third  test', () => {
  cy.visit('/checkbox');

  const expandAndCheckChildNode = (node, childNode) => {
    cy.xpath(
      `//label[@for='tree-node-${node}']/../button[contains(@class, 'rct-collapse')]`
    ).click();
    cy.xpath(`//label[@for='tree-node-${childNode}']`).should('be.visible');
  };

  expandAndCheckChildNode('home', 'documents');
  expandAndCheckChildNode('documents', 'workspace');
  expandAndCheckChildNode('workspace', 'react');

  cy.xpath("//label[@for='tree-node-react']").click();

  cy.get('div#result').within(() => {
    cy.get('span').contains('You have selected :').should('be.visible');
    cy.get('span.text-success').contains('react').should('be.visible');
  });
});

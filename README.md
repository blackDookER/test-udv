Для установки пакетов: npm install

Для запуска: npm run cypress

В открывшимся окне выбираем e2e testing,
Start e2e testing in chrome, дальше выбираем нужный нам тест - "TZ"

Переменная окружения baseUrl находится в cypress.config.js

Дефолтный таймаут для селекторов в конфиге 20 секунд

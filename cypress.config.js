const fs = require('fs');

module.exports = {
  env: {
    shouldVisitMonitoring: true,
  },

  e2e: {
    baseUrl: 'https://demoqa.com',

    setupNodeEvents(on, config) {
      on('task', {
        // deconstruct the individual properties
        removeFolder(path) {
          try {
            fs.rmdirSync(path, { maxRetries: 10, recursive: true });
          } catch (e) {}
          return null;
        },
      });
    },
  },
  defaultCommandTimeout: 65000,
};
